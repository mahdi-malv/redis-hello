import redis.clients.jedis.Jedis;

public class Redis {

    public static Jedis getInstance() {
        return new Jedis("localhost");
    }

    public static Jedis getInstance(String serverIp) {
        return new Jedis(serverIp);
    }

    public static Jedis getInstance(String serverIp, int port) {
        return new Jedis(serverIp, port);
    }

}
