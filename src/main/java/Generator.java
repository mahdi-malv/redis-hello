import redis.clients.jedis.Jedis;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Generator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("[Note]: Make sure redis is running");
        System.out.print("Enter number: ");
        int number = scanner.nextInt();
        Jedis j = Redis.getInstance();

        // If 1, OK (Added), if 0 (Exists) replacing
        for (int i = 0; i < number; i++) {
            long status = j.sadd("mySet", scanner.next());
            if (status == 1) {
                System.out.println("OK");
            } else {
                System.out.println("Replacing");
            }
        }

        // Get the set
        Set<String> set = j.smembers("mySet");

        System.out.println("--- Printing Members of set ---");

        // To be able to use int inside the lambda, using Atomic
        AtomicInteger i = new AtomicInteger();
        set.iterator().forEachRemaining(s -> {
            System.out.println("Member#" + i + " : " + s);
            i.getAndIncrement();
        });

    }
}
