import redis.clients.jedis.Jedis;

public class HelloRedis {

    public static void main(String[] args) {
        try {
            Jedis jedis = Redis.getInstance();
            System.out.println("The server is running " + jedis.ping());
            jedis.set("Golnoosh", "Hello, Redis!");
            System.out.println("Stored string in redis:: " + jedis.get("Golnoosh"));

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
