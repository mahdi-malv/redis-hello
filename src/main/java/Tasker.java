import redis.clients.jedis.Jedis;

import java.util.Scanner;

public class Tasker {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of tasks: ");
        int number = scanner.nextInt();
        Jedis j = Redis.getInstance();
        for (int i = 0; i < number; i++) {
            j.lpush("tasks", scanner.next());
        }

        try {
            while (true) {
                String task = j.lpop("tasks");
                if (task != null) {
                    System.out.println("Running task: " + task);
                    Thread.sleep(1000);
                } else {
                    System.out.println("NULL");
                    break;
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Error occurred using sleep function to handle task!");
        }
    }
}
